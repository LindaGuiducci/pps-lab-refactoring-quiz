package com.geoquiz.controller.account;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.geoquiz.utility.LocalFolder;
import com.geoquiz.utility.Pair;

/**
 * Implementation of Account interface.
 *
 */
public final class AccountImpl implements Account {

    public final static String ILLEGAL_STATE_EXCEPTION_MESSAGE = "Username already exists!!";
    public final static String ILLEGAL_ARGUMENT_EXCEPTION_MESSAGE = "USERNAME O PASSWORD ERRATI!";

    private final File file;
    private Set<Pair<String, String>> users = new HashSet<>();
    private Optional<Pair<String, String>> currentUser = Optional.empty();

    /**
     * @param fileName
     *            the name of the file.
     * @throws IOException
     *             exception.
     */
    public AccountImpl(final String fileName) throws IOException {
        this.file = new File(LocalFolder.getLocalFolderPath() + LocalFolder.getFileSeparator() + fileName);
        file.createNewFile();
        this.read(this.file.toString());
    }

    @Override
    public void register(final Pair<String, String> idPassword) throws IllegalStateException {
        if (checkUsername(idPassword)) {
            users.add(idPassword);
            this.save();
        }
    }

    private boolean checkUsername (final Pair<String, String> idPassword) throws IllegalStateException {
        for (final Pair<String, String> p : users) {
            if (idPassword.getX().equals(p.getX())) {
                // USERNAME ALREADY EXISTS
                throw new IllegalStateException(ILLEGAL_STATE_EXCEPTION_MESSAGE);
            }
        }
        return true;
    }


    @Override
    public void checkLogin(final String id, final String password) throws IllegalArgumentException {
        final Pair<String, String> user = new Pair<>(id, password);
        if (this.users.contains(user)) {
            this.currentUser = Optional.of(user);
        } else {
            throw new IllegalArgumentException(ILLEGAL_ARGUMENT_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public Optional<String> getLoggedPlayerID() {
        return Optional.of(this.currentUser.get().getX());
    }

    @Override
    public void logout() {
        this.currentUser = Optional.empty();

    }

    @Override
    public void read(final String fileName) throws IOException {

        this.users = Files.lines(Paths.get(fileName)).filter(l -> l.trim().length() > 0).map(l -> l.split(","))
                .map(a -> new Pair<>(a[0].trim(), a[1].trim())).collect(Collectors.toSet());
    }

    @Override
    public void save() {
        try {
            final PrintStream stream = new PrintStream(this.file);
            users.forEach(p -> stream.println(this.myToString(p)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void removeAllAccount() {

        this.users.clear();
        this.save();

    }

    private String myToString(final Pair<String, String> pair) {
        return pair.getX() + "," + pair.getY();

    }

}
