package com.geoquiz.controller.account;

import java.io.IOException;

/**
 * FileOperation interface that provides read, save and removeAllAccount methods.
 *
 */
public interface FileOperation {

    /**
     * Read from file.
     * 
     * @param fileName
     *            the name of the file.
     * @throws IOException
     *             exception.
     */
    void read(String fileName) throws IOException;

    /**
     * Save on file.
     */
    void save();

    /**
     * Remove all player's account.
     */
    void removeAllAccount();

}
